import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'

admin.initializeApp(functions.config().functions)

exports.deleteDatedBookings = functions.firestore
.document('/bookings/{bookingsId}')
.onCreate((change, context) => {

    const db = admin.firestore()
    
    // Get the current time - 10 minutes
    let minutes = 600000
    let dateTimeNow = new Date(Date.now() - minutes)
    console.log(dateTimeNow)

    // Convert date time to timestamp for Firestore query
    const timestamp = admin.firestore.Timestamp.fromDate(dateTimeNow)
    let newData = db.batch()

    // Make query and remove outdated bookings
    return db
    .collection('bookings')
    .where('start_time_date', '<=', timestamp)
    .get()
    .then(snapshot => {
        snapshot.forEach(doc => {
            newData.delete(doc.ref)
        })
        return newData.commit()
    })
    .catch(err => {
        console.error('error occurred', err)
    })
})