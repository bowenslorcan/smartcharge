import '../imports.dart';
import 'package:intl/intl.dart';

class ViewBookingsPage extends StatefulWidget {
  final userEmail;

  ViewBookingsPage({this.userEmail});

  @override
  _ViewBookingsPageState createState() => _ViewBookingsPageState();
}

class _ViewBookingsPageState extends State<ViewBookingsPage> {
  String _message;
  CrudServices _servicesObj = CrudServices();

  @override
  Widget build(BuildContext context) {
    final _encryptedEmail = AesService().encrypter(widget.userEmail);

    return Container(
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height - 62.0,
      child: StreamBuilder(
          stream: _servicesObj.getUserBookings(_encryptedEmail),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Spinner();
            } else if (snapshot.data.documents.length > 0) {
              return ListView(
                children: <Widget>[
                  Constants.bookingsTitle(),
                  ListView(
                    primary: false,
                    shrinkWrap: true,
                    padding: EdgeInsets.only(left: 25.0, right: 25.0),
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height - 300.0,
                        child: ListView.builder(
                          itemCount: snapshot.data.documents.length,
                          itemBuilder: (_, index) {
                            return _buildBookings(snapshot, index);
                          },
                        ),
                      )
                    ],
                  )
                ],
              );
            } else {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Constants.bookingsTitle(),
                  SizedBox(
                    height: 20.0,
                  ),
                  FadeAnimation(
                    1.0,
                    Container(
                      width: 190.0,
                      height: 140.0,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/images/busy_bee.gif'))),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  FadeAnimation(
                    1.5,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Flexible(
                            child: Text(
                          'Trust us, we ',
                          style: TextStyle(
                              color: Color(0xFF0D2A47),
                              fontSize: 19.0,
                              fontFamily: 'Monstserrat',
                              fontWeight: FontWeight.w500),
                        )),
                        Flexible(
                          child: Text('KNOW ',
                              style: TextStyle(
                                  color: Color(0xFF4ABD61),
                                  fontSize: 19.0,
                                  fontFamily: 'Monstserrat',
                                  fontWeight: FontWeight.w500)),
                        ),
                        Flexible(
                          child: Text("you're busy!",
                              style: TextStyle(
                                  color: Color(0xFF0D2A47),
                                  fontSize: 19.0,
                                  fontFamily: 'Monstserrat',
                                  fontWeight: FontWeight.w500)),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Flexible(
                          child: Text(
                        'Let us ',
                        style: TextStyle(
                            color: Color(0xFF0D2A47),
                            fontSize: 19.0,
                            fontFamily: 'Monstserrat',
                            fontWeight: FontWeight.w500),
                      )),
                      Flexible(
                        child: Text('EASE ',
                            style: TextStyle(
                                color: Color(0xFF4ABD61),
                                fontSize: 19.0,
                                fontFamily: 'Monstserrat',
                                fontWeight: FontWeight.w500)),
                      ),
                      Flexible(
                          child: Text(
                        'that schedule',
                        style: TextStyle(
                            color: Color(0xFF0D2A47),
                            fontSize: 19.0,
                            fontFamily: 'Monstserrat',
                            fontWeight: FontWeight.w500),
                      )),
                      SizedBox(
                        width: 5.0,
                      ),
                      Container(
                        height: 30.0,
                        width: 30.0,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image:
                                    AssetImage('assets/images/help_out.png'))),
                      ),
                    ],
                  )
                ],
              );
            }
          }),
    );
  }

  Widget _buildBookings(AsyncSnapshot snapshot, int index) {
    final bookingDetails = snapshot.data.documents[index];
    final dateTime =
        DateTime.parse(bookingDetails.data['created_on'].toDate().toString());
    final formattedReservationDate = DateFormat('EEE d MMM').format(dateTime);
    return Container(
      padding: EdgeInsets.only(bottom: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width - 100.0,
            child: FadeAnimation(
              1.0,
              Card(
                child: Container(
                  padding: EdgeInsets.only(left: 10.0, right: 10.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 20.0,
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            height: 30.0,
                            width: 30.0,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage(
                                        'assets/images/location_icon.png'))),
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Flexible(
                            child: Text(
                                'Location : ' +
                                    bookingDetails.data['charge_point_name']
                                        .toString(),
                                style: Constants.informationTextStyleBody()),
                          ),
                        ],
                      ),
                      SizedBox(height: 10.0),
                      Row(
                        children: <Widget>[
                          Container(
                            height: 30.0,
                            width: 30.0,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage(
                                        'assets/images/clock_icon.png'))),
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Text(
                              'Duration : ' +
                                  bookingDetails.data['start_time'].toString() +
                                  ' - ' +
                                  bookingDetails.data['end_time'].toString(),
                              style: Constants.informationTextStyleBody()),
                        ],
                      ),
                      Separator(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                height: 30.0,
                                width: 30.0,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage(
                                            'assets/images/reserved_date_icon.png'))),
                              ),
                              SizedBox(
                                width: 10.0,
                              ),
                              Text('Reserved : ' + formattedReservationDate,
                                  style: Constants.informationTextStyleBody()),
                            ],
                          ),
                          InkWell(
                            onTap: () {
                              _servicesObj.deleteBooking(snapshot, index);
                              setState(() {
                                _message = 'Your booking has been deleted!';
                                return showSuccessFlushbar(context, _message);
                              });
                            },
                            child: Container(
                              height: 30.0,
                              width: 30.0,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          'assets/images/delete_icon.png'))),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20.0,
                      )
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
