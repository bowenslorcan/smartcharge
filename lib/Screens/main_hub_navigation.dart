import '../imports.dart';

class MainHub extends StatefulWidget {
  @override
  _MainHubState createState() => _MainHubState();
}

class _MainHubState extends State<MainHub> {
  // Getting current user
  FirebaseUser currentUser;

  String _message;

  CrudServices _servicesObj = CrudServices();

  void _loadCurrentUser() {
    FirebaseAuth.instance.currentUser().then((FirebaseUser user) {
      WidgetsBinding.instance.addPostFrameCallback((_) => setState(() {
            this.currentUser = user;
            _greeting();
          }));
    });
  }

  String _email() {
    if (currentUser != null) {
      return currentUser.email;
    } else {
      return "Not Signed In!";
    }
  }

  int _page = 0;

  @override
  void initState() {
    super.initState();
    _loadCurrentUser();
  }

  @override
  Widget build(BuildContext context) {
    final _pageSelector = [
      MapPage(userEmail: _email()),
      ViewBookingsPage(userEmail: _email()),
    ];

    return Scaffold(
      backgroundColor: Color(0xFF51CBFF),
//      backgroundColor: Color(0xFFE0EAFB),
      appBar: AppBar(
        backgroundColor: Color(0xFF4ABD61),
        automaticallyImplyLeading: false,
        title: Row(
          children: <Widget>[
            Text(
              'Smart',
              style: TextStyle(
                  color: Color(0xFF0D2A47),
                  fontSize: 25.0,
                  fontFamily: 'Monstserrat',
                  fontWeight: FontWeight.w500),
            ),
            Text(
              'Charge',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 25.0,
                  fontFamily: 'Monstserrat',
                  fontWeight: FontWeight.w500),
            )
          ],
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(top: 10.0),
        child: CurvedNavigationBar(
          backgroundColor: Color(0xFF51CBFF),
          index: 0,
          buttonBackgroundColor: Color(0xFF51CBFF),
          height: 62,
          items: <Widget>[
            Container(
              height: 50.0,
              width: 50.0,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/map_icon.png'))),
            ),
            Container(
              height: 50.0,
              width: 50.0,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/booking_icon.png'))),
            ),
            Container(
              height: 50.0,
              width: 50.0,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/logout_icon.png'))),
            )
          ],
          animationDuration: Duration(milliseconds: 200),
          animationCurve: Curves.bounceInOut,
          onTap: (index) {
            if (index == 2) {
              _servicesObj.logout();
              Navigator.pushReplacementNamed(context, '/');
            } else {
              setState(() {
                _page = index;
              });
            }
          },
        ),
      ),
      body: _pageSelector[_page],
    );
  }

  _greeting() async {
    final _encryptedEmail = AesService().encrypter(_email());
    try {
      _servicesObj.getUser(_encryptedEmail).then((user) {
        final decryptedUser =
            AesService().decrypter(user.documents[0].data['forename']);
        setState(() {
          _message = 'Welcome back $decryptedUser!';
          showSuccessFlushbar(context, _message);
        });
      });
    } catch (e) {
      print(e);
    }
  }
}
