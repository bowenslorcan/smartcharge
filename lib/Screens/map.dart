import '../imports.dart';
import 'package:location/location.dart' as Tracking;

class MapPage extends StatefulWidget {
  final userEmail;

  MapPage({this.userEmail});

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  // Form validation
  final _addressValue = new TextEditingController();

  // Services
  CrudServices _servicesObj = CrudServices();

  // Live location tracking
  StreamSubscription _locationSubscription;
  Tracking.Location _locationTracker = Tracking.Location();
  var _currentLocation;

  // Get initial camera position
  static final CameraPosition _initialPosition =
      CameraPosition(target: LatLng(53.1424, -7.6921), zoom: 8.0);

  // Getting custom markers
  BitmapDescriptor _pointAvailableIcon;
  BitmapDescriptor _pointOccupiedIcon;
  BitmapDescriptor _markerUserIcon;

  // Conditional Widgets
  bool _routeDetailsToggle = false;
  bool _mapToggle = false;

  // Empty list for search query
  var _queryResultSet = [];
  var _tempSearchStore = [];

  // Empty list for distance matrix results
  var _distanceMatrix = [];

  // Map for holding all markers
  Map<MarkerId, Marker> _markers = <MarkerId, Marker>{};
  Marker _userMarker;
  Circle _userCircle;

  GoogleMapController _mapController;

  /*
    Title: Flutter - Google Maps - Drawing Routes with Polylines
    Author: Raja Yogan
    Date: Nov 28, 2019
    Code version: 1
    Availability: https://www.youtube.com/watch?v=rvXRc1zwFpQ
  */

  // Tools for calculating polylines
  final Set<Polyline> _route = {};
  List<LatLng> _routeCoordinates;
  GoogleMapPolyline _googleMapPolyline =
      new GoogleMapPolyline(apiKey: "AIzaSyBnV5_2E6SkunNQ2JwbrR7_1XUQK8s8cMo");

  getPoints(currentLocation, chargePointLocation, polylineID) async {
    _routeCoordinates = await _googleMapPolyline.getCoordinatesWithLocation(
        origin: currentLocation,
        destination: chargePointLocation,
        mode: RouteMode.driving);

    if (_routeCoordinates.length > 0) {
      _buildPolyline(_routeCoordinates, polylineID);
    } else {
      _routeCoordinates.clear();
      _buildPolyline(_routeCoordinates, polylineID);
    }
  }

  // Initialising state, defining map display, creating connection to Firestore
  @override
  void initState() {
    super.initState();
    _addressValue.text = '';
    Geolocator().getCurrentPosition().then((currloc) {
      setState(() {
        getCurrentLocation();
        initMarkerIcon();
        chargePoints();
        _mapToggle = true;
      });
    });
  }

  // Main page layout
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<QuerySnapshot>(
          stream: _servicesObj.getChargePoints(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (!snapshot.hasData) {
              return Spinner();
            }

            snapshot.data.documents.forEach((change) {
              var markerIdVal = change.documentID;
              final MarkerId markerId = MarkerId(markerIdVal);

              if (change.data['occupied'] == true) {
                _markers[markerId] = Marker(
                    markerId: markerId,
                    icon: _pointOccupiedIcon,
                    position: LatLng(change.data['location'].latitude,
                        change.data['location'].longitude),
                    infoWindow: InfoWindow(
                        title: change.data['name'],
                        snippet: change.data['type']),
                    onTap: () {
                      PointDetailsViewModel()
                          .findNextAvailableBooking(change.documentID)
                          .then((val) {
                        pointInfo(context, change.data, change.documentID, val);
                      });
                    });
              } else {
                _markers[markerId] = Marker(
                    markerId: markerId,
                    icon: _pointAvailableIcon,
                    position: LatLng(change.data['location'].latitude,
                        change.data['location'].longitude),
                    infoWindow: InfoWindow(
                        title: change.data['name'],
                        snippet: change.data['type']),
                    onTap: () {
                      PointDetailsViewModel()
                          .findNextAvailableBooking(change.documentID)
                          .then((val) {
                        pointInfo(context, change.data, change.documentID, val);
                      });
                    });
              }
            });

            return FadeAnimation(
                1,
                Stack(
                  children: <Widget>[
                    Container(
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        child: _mapToggle
                            ? GoogleMap(
                                // Using geolocation to identify where the user is
                                onMapCreated: onMapCreated,
                                compassEnabled: false,
                                polylines: _route,
                                initialCameraPosition: _initialPosition,
                                markers: Set<Marker>.of(_markers.values),
                                circles: Set.of(
                                    (_userCircle != null) ? [_userCircle] : []),
                              )
                            : Spinner()),
                    Positioned(
                      top: 30.0,
                      right: 15.0,
                      left: 15.0,
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: 50.0,
                            width: double.infinity,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(10.0),
                                    topLeft: Radius.circular(10.0)),
                                color: Colors.white),
                            child: TextField(
                              controller: _addressValue,
                              onChanged: (val) {
                                RegExp exp = RegExp(r'^[a-zA-Z0-9, ]*$');
                                // Security conditions for initiating safe search
                                if (exp.hasMatch(val)) {
                                  initiateSearch(val);
                                } else {
                                  val.trim();
                                  _addressValue.text =
                                      val.substring(0, val.length - 1);
                                }
                              },
                              style: Constants.informationTextStyleBody(),
                              decoration: InputDecoration(
                                hintText: 'Charge Point Address...',
                                hintStyle: Constants.hintTextStyle(),
                                border: InputBorder.none,
                                contentPadding:
                                    EdgeInsets.only(left: 15.0, top: 15.0),
                                suffixIcon: Icon(
                                  Icons.search,
                                  color: Colors.black45,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10.0),
                                    bottomLeft: Radius.circular(10.0)),
                                color: Colors.white),
                            child: ListView(
                              shrinkWrap: true,
                              children: _tempSearchStore.map((element) {
                                return buildResultList(context, element);
                              }).toList(),
                            ),
                          ),
                        ],
                      ),
                    ),
                    _buildRouteDetails(context, _distanceMatrix),
                  ],
                ));
          }),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(bottom: 50.0, right: 5.0),
        child: FloatingActionButton(
          backgroundColor: Color(0xFF51CBFF),
          elevation: 0,
          child: Icon(Icons.location_searching),
          onPressed: () {
            updateCameraPosition();
          },
        ),
      ),
    );
  }

  Future<bool> pointInfo(context, chargePoint, documentID, availability) {
    return showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            color: Color(0xFF737373),
            height: 480.0,
            child: Container(
              child: _buildPointDetails(chargePoint, documentID, availability),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(10),
                      topRight: const Radius.circular(10))),
            ),
          );
        });
  }

  Widget _buildPointDetails(chargePoint, documentID, availability) {
    FocusScope.of(context).unfocus();
    return Center(
      child: Container(
        width: 380.0,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Container(
              child: Icon(
                Icons.maximize,
                size: 35.0,
                color: Colors.black12,
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 5.0, bottom: 10.0),
              child: Text(chargePoint['name'],
                  style: Constants.informationTextStyleHeading()),
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              padding: EdgeInsets.only(top: 5.0, right: 5.0, bottom: 5.0),
              child: Row(
                children: <Widget>[
                  Container(
                    height: 30.0,
                    width: 30.0,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/images/type.png'))),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Text('Charge Point Type : ' + chargePoint['type'],
                      style: Constants.informationTextStyleBody()),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 5.0, right: 5.0, bottom: 5.0),
              child: Row(
                children: <Widget>[
                  Container(
                    height: 30.0,
                    width: 30.0,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image:
                                AssetImage('assets/images/question_mark.png'))),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Flexible(
                    child: Text('Next Available : ' + availability,
                        style: Constants.informationTextStyleBody()),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              width: 380,
              child: FlatButton(
                shape: new RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  side: BorderSide(color: Color(0xFF4ABD61)),
                ),
                color: Color(0xFF4ABD61),
                textColor: Colors.white,
                padding: EdgeInsets.all(15.0),
                onPressed: () {
                  print(chargePoint['name']);
                  Navigator.pop(context);
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => CreateReservation(
                        chargePoint: documentID,
                        userEmail: widget.userEmail,
                        chargeType: chargePoint['type'],
                        chargePointName: chargePoint['name']),
                  ));
                },
                child: Text("Reserve", style: Constants.buttonTextStyle()),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              width: 380,
              child: FlatButton(
                shape: new RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  side: BorderSide(color: Color(0xFF4ABD61)),
                ),
                color: Colors.white,
                textColor: Color(0xFF4ABD61),
                padding: EdgeInsets.all(15.0),
                onPressed: () {
                  return showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return ViewAllBookingsDialog(
                            chargePointID: documentID,
                            pointName: chargePoint['name']);
                      });
                },
                child:
                    Text("View Bookings", style: Constants.buttonTextStyle()),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              width: 380,
              child: FlatButton(
                shape: new RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  side: BorderSide(color: Color(0xFF2196F3)),
                ),
                color: Color(0xFF2196F3),
                textColor: Colors.white,
                padding: EdgeInsets.all(15.0),
                onPressed: () {
                  Navigator.pop(context);

                  GeoPoint location = chargePoint['location'];
                  LatLng pointLatLng =
                      new LatLng(location.latitude, location.longitude);
                  LatLng current = new LatLng(
                      _currentLocation.latitude, _currentLocation.longitude);
                  getPoints(current, pointLatLng, chargePoint['name']);

                  String currentMatrixCoord =
                      _currentLocation.latitude.toString() +
                          ', ' +
                          _currentLocation.longitude.toString();
                  String chargePointMatrixCoord = location.latitude.toString() +
                      ', ' +
                      location.longitude.toString();

                  RouteDetailsViewModel()
                      .getDistance(currentMatrixCoord, chargePointMatrixCoord,
                          chargePoint['name'])
                      .then((val) {
                    _distanceMatrix = val;
                  });

                  updateCameraPosition();

                  _widgetToggle();
                },
                child: Text("Start Route", style: Constants.buttonTextStyle()),
              ),
            ),
            SizedBox(
              height: 20.0,
            )
          ],
        ),
      ),
    );
  }

  Widget _buildRouteDetails(BuildContext context, element) {
    if (element.isNotEmpty) {
      return Visibility(
        visible: _routeDetailsToggle,
        child: Container(
          color: Colors.white,
          height: 200.0,
          child: Center(
            child: Container(
              color: Colors.white,
              width: MediaQuery.of(context).size.width - 100.0,
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 5.0, right: 5.0, bottom: 5.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: 30.0,
                          width: 30.0,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/charge_point.png'))),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Flexible(
                          child: Text('Charge Point : ' + element[2],
                              style: Constants.informationTextStyleBody()),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 5.0, right: 5.0, bottom: 5.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: 30.0,
                          width: 30.0,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/distance.png'))),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text('Distance : ' + element[0],
                            style: Constants.informationTextStyleBody()),
                        SizedBox(
                          width: 20.0,
                        ),
                        Container(
                          height: 30.0,
                          width: 30.0,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/clock_icon.png'))),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text('Duration : ' + element[1],
                            style: Constants.informationTextStyleBody()),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    width: 350,
                    child: FlatButton(
                      shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        side: BorderSide(color: Colors.red),
                      ),
                      color: Colors.red,
                      textColor: Colors.white,
                      padding: EdgeInsets.all(15.0),
                      onPressed: () {
                        setState(() {
                          _route.clear();
                          _distanceMatrix = [];
                          _widgetToggle();
                        });
                      },
                      child: Text("Cancel Route",
                          style: Constants.buttonTextStyle()),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  void updateCameraPosition() async {
    LatLng latLng =
        LatLng(_currentLocation.latitude, _currentLocation.longitude);
    _mapController.animateCamera(CameraUpdate.newCameraPosition(
        new CameraPosition(target: latLng, tilt: 0, bearing: 192, zoom: 11.0)));
  }

  /*
    Title: Flutter Google Maps and Live Location Tracking | Flutter Maps Tutorial
    Author: RetroPortal Studio
    Date: Jan 25, 2020
    Code version: 1
    Availability: https://www.youtube.com/watch?v=McPzVZZRniU
  */

  void getCurrentLocation() async {
    try {
      var markerIdVal = 'home';
      final MarkerId markerId = MarkerId(markerIdVal);

      getUserMarker();
      var location = await _locationTracker.getLocation();

      if (_userMarker == null) {
        LatLng latLng = LatLng(location.latitude, location.longitude);
        _userMarker = Marker(
            markerId: markerId,
            position: latLng,
            rotation: location.heading,
            draggable: false,
            zIndex: 2,
            flat: true,
            anchor: Offset(0.5, 0.5),
            icon: _markerUserIcon);

        setState(() {
          _markers[markerId] = _userMarker;
        });
      }

      updateUserMarkerAndCircle(location);

      if (_locationSubscription != null) {
        _locationSubscription.cancel();
      }

      _locationSubscription =
          _locationTracker.onLocationChanged.listen((newLocationData) {
        if (_mapController != null) {
          updateUserMarkerAndCircle(newLocationData);
          _currentLocation = newLocationData;
        }
      });
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        print('Permission Denied');
      }
    }
  }

  void dispose() {
    if (_locationSubscription != null) {
      _locationSubscription.cancel();
    }
    super.dispose();
  }

  updateUserMarkerAndCircle(Tracking.LocationData newLocationData) {
    LatLng latLng = LatLng(newLocationData.latitude, newLocationData.longitude);

    this.setState(() {
      _markers[MarkerId('home')] = Marker(
          markerId: MarkerId('home'),
          position: latLng,
          rotation: newLocationData.heading,
          draggable: false,
          zIndex: 2,
          flat: true,
          anchor: Offset(0.5, 0.5),
          icon: _markerUserIcon);

      _userCircle = Circle(
          circleId: CircleId('car'),
          radius: newLocationData.accuracy,
          zIndex: 1,
          strokeColor: Colors.blue,
          center: latLng,
          fillColor: Colors.blue.withAlpha(70));
    });
  }

  // Grabbing charge points from Firestore
  chargePoints() {
    _servicesObj.initialiseChargePoints().then((docs) {
      if (docs.documents.isNotEmpty) {
        for (int i = 0; i < docs.documents.length; i++) {
          initMarker(docs.documents[i].data, docs.documents[i].documentID);
        }
      }
    });
  }

  void initMarker(chargePoint, documentID) {
    var markerIdVal = documentID;
    final MarkerId markerId = MarkerId(markerIdVal);
    Marker marker;

    if (chargePoint['occupied'] == true) {
      // Creating a new Charge Point Marker
      marker = Marker(
        markerId: markerId,
        icon: _pointOccupiedIcon,
        position: LatLng(chargePoint['location'].latitude,
            chargePoint['location'].longitude),
        infoWindow: InfoWindow(
            title: chargePoint['name'], snippet: chargePoint['type']),
      );
    } else {
      // Creating a new Charge Point Marker
      marker = Marker(
        markerId: markerId,
        icon: _pointAvailableIcon,
        position: LatLng(chargePoint['location'].latitude,
            chargePoint['location'].longitude),
        infoWindow: InfoWindow(
            title: chargePoint['name'], snippet: chargePoint['type']),
      );
    }

    setState(() {
      _markers[markerId] = marker;
    });
  }

  getUserMarker() {
    if (_markerUserIcon == null) {
      ImageConfiguration configuration = createLocalImageConfiguration(context);
      BitmapDescriptor.fromAssetImage(
              configuration, 'assets/images/current_location_icon.png')
          .then((icon) {
        setState(() {
          _markerUserIcon = icon;
        });
      });
    }
  }

  initMarkerIcon() {
    if (_pointAvailableIcon == null) {
      ImageConfiguration configuration = createLocalImageConfiguration(context);
      BitmapDescriptor.fromAssetImage(
              configuration, 'assets/images/available_point_icon.png')
          .then((icon) {
        setState(() {
          _pointAvailableIcon = icon;
        });
      });
    }
    if (_pointOccupiedIcon == null) {
      ImageConfiguration configuration = createLocalImageConfiguration(context);
      BitmapDescriptor.fromAssetImage(
              configuration, 'assets/images/occupied_point_icon.png')
          .then((icon) {
        setState(() {
          _pointOccupiedIcon = icon;
        });
      });
    }
  }

  // Controlling map display
  void onMapCreated(GoogleMapController controller) {
    setState(() {
      _mapController = controller;
    });
  }

  /*
    Title: Flutter - Instant search with Firestore
    Author: Raja Yogan
    Date: Nov 27, 2018
    Code version: 1
    Availability: https://www.youtube.com/watch?v=0szEJiCUtMM
  */

  // Initiate search for Charge Point
  void initiateSearch(value) {
    // Resetting list to provide fresh set of results
    if (value.length == 0) {
      setState(() {
        _queryResultSet = [];
        _tempSearchStore = [];
      });
    }

    // Variable used to always capitalise the first value inputted by the user
    var capitalizedValue =
        value.substring(0, 1).toUpperCase() + value.substring(1);

    if (_queryResultSet.length == 0 && value.length == 1) {
      _servicesObj.searchByName(value).then((QuerySnapshot docs) {
        for (int i = 0; i < docs.documents.length; i++) {
          _queryResultSet.add(docs.documents[i].data);
        }
      });
    } else {
      _tempSearchStore = [];
      _queryResultSet.forEach((element) {
        if (element['name'].startsWith(capitalizedValue)) {
          setState(() {
            _tempSearchStore.add(element);
          });
        }
      });

      if (_tempSearchStore.length == 0 && value.length > 1) {
        setState(() {});
      }
    }
  }

  void _widgetToggle() {
    setState(() {
      _routeDetailsToggle = !_routeDetailsToggle;
    });
  }

  void _buildPolyline(coordinates, chargePoint) {
    setState(() {
      _route.add(Polyline(
          polylineId: PolylineId(chargePoint),
          visible: true,
          points: coordinates,
          color: Color(0xFF2196F3)));
    });
  }

  Widget buildResultList(BuildContext context, data) {
    return Container(
      padding:
          EdgeInsets.only(left: 15.0, right: 15.0, top: 10.5, bottom: 10.5),
      child: InkWell(
        onTap: () {
          setState(() {
            _addressValue.clear();

            // Resetting query results
            _queryResultSet = [];
            _tempSearchStore = [];

            GeoPoint location = data['location'];

            // Creating a new LatLng object to store the GeoPoint received
            LatLng latLng = new LatLng(location.latitude, location.longitude);

            _mapController.moveCamera(CameraUpdate.newLatLngZoom(latLng, 18));
          });
        },
        child: Text(data['name'], style: Constants.informationTextStyleBody()),
      ),
    );
  }
}
