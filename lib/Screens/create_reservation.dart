import '../imports.dart';

class CreateReservation extends StatefulWidget {
  final chargePoint;
  final userEmail;
  final chargePointName;
  final chargeType;

  CreateReservation(
      {Key key,
      @required this.chargePoint,
      this.userEmail,
      this.chargeType,
      this.chargePointName})
      : super(key: key);

  @override
  _CreateReservationState createState() => _CreateReservationState(
      chargePoint, userEmail, chargeType, chargePointName);
}

class _CreateReservationState extends State<CreateReservation> {
  String _chargePoint, _message, _userEmail, _chargeType, _chargePointName;

  _CreateReservationState(this._chargePoint, this._userEmail, this._chargeType,
      this._chargePointName);

  TimeOfDay _startTime = TimeOfDay.now();
  TimeOfDay _endTime = TimeOfDay.now();
  TimeOfDay _startPicked, _endPicked;

  CrudServices _servicesObj = new CrudServices();

  @override
  void initState() {
    super.initState();
  }

  Future<void> selectStartTime(BuildContext context) async {
    _startPicked =
        await showTimePicker(context: context, initialTime: _startTime);

    if (_startPicked != null) {
      setState(() {
        _startTime = _startPicked;
      });
    }
  }

  Future<void> selectEndTime(BuildContext context) async {
    _endPicked = await showTimePicker(context: context, initialTime: _endTime);

    if (_endPicked != null) {
      setState(() {
        _endTime = _endPicked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final MaterialLocalizations localizations =
        MaterialLocalizations.of(context);
    final String formattedStartTime = localizations.formatTimeOfDay(_startTime);
    final String formattedEndTime = localizations.formatTimeOfDay(_endTime);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color(0xFF4ABD61),
        automaticallyImplyLeading: false,
        title: Row(
          children: <Widget>[
            Text(
              'Smart',
              style: TextStyle(
                  color: Color(0xFF0D2A47),
                  fontSize: 25.0,
                  fontFamily: 'Monstserrat',
                  fontWeight: FontWeight.w500),
            ),
            Text(
              'Charge',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 25.0,
                  fontFamily: 'Monstserrat',
                  fontWeight: FontWeight.w500),
            )
          ],
        ),
      ),
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width - 100,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FadeAnimation(
                1.0,
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: 59,
                      width: 160,
                      child: FlatButton(
                        shape: new RoundedRectangleBorder(
                          side: BorderSide(color: Color(0xFF2196F3)),
                        ),
                        color: Colors.white,
                        textColor: Color(0xFF2196F3),
                        onPressed: () {
                          selectStartTime(context);
                        },
                        child: Text(
                          "Start Time".toUpperCase(),
                          style: TextStyle(
                              fontSize: 16.0,
                              fontFamily: 'Monstserrat',
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                    Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 10.0),
                        height: 59,
                        width: 320,
                        decoration: BoxDecoration(
                            border: Border.all(color: Color(0xFF2196F3))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              formattedStartTime,
                              style: TextStyle(
                                  color: Color(0xFF2196F3),
                                  fontSize: 16.0,
                                  fontFamily: 'Monstserrat',
                                  fontWeight: FontWeight.w300),
                            ),
                            Container(
                              padding: EdgeInsets.only(right: 10.0),
                              child: Icon(
                                Icons.alarm,
                                color: Color(0xFF2196F3),
                              ),
                            )
                          ],
                        )),
                  ],
                ),
              ),
              SizedBox(
                height: 35.0,
              ),
              FadeAnimation(
                1.4,
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: 59,
                      width: 160,
                      child: FlatButton(
                        shape: new RoundedRectangleBorder(
                          side: BorderSide(color: Color(0xFF2196F3)),
                        ),
                        color: Colors.white,
                        textColor: Color(0xFF2196F3),
                        onPressed: () {
                          selectEndTime(context);
                        },
                        child: Text(
                          "End Time".toUpperCase(),
                          style: TextStyle(
                              fontSize: 16.0,
                              fontFamily: 'Monstserrat',
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                    Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 10.0),
                        height: 59,
                        width: 320,
                        decoration: BoxDecoration(
                            border: Border.all(color: Color(0xFF2196F3))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              formattedEndTime,
                              style: TextStyle(
                                  color: Color(0xFF2196F3),
                                  fontSize: 16.0,
                                  fontFamily: 'Monstserrat',
                                  fontWeight: FontWeight.w300),
                            ),
                            Container(
                              padding: EdgeInsets.only(right: 10.0),
                              child: Icon(
                                Icons.alarm,
                                color: Color(0xFF2196F3),
                              ),
                            )
                          ],
                        )),
                  ],
                ),
              ),
              SizedBox(
                height: 50.0,
              ),
              FadeAnimation(
                1.8,
                Container(
                  width: 480,
                  child: FlatButton(
                    shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      side: BorderSide(color: Color(0xFF4ABD61)),
                    ),
                    color: Color(0xFF4ABD61),
                    textColor: Colors.white,
                    padding: EdgeInsets.all(20.0),
                    onPressed: () {
                      CreateReservationViewModel()
                          .bookingValidation(
                              formattedStartTime,
                              formattedEndTime,
                              _startPicked,
                              _endPicked,
                              _chargePoint,
                              _chargeType)
                          .then((val) {
                        if (val != 'OK') {
                          print(val);
                          setState(() {
                            return showErrorFlushbar(context, val);
                          });
                        } else {
                          _submitBooking(formattedStartTime, formattedEndTime);
                        }
                      });
                    },
                    child: Text(
                      "Confirm",
                      style: TextStyle(
                          fontSize: 16.0,
                          fontFamily: 'Monstserrat',
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              FadeAnimation(
                2.2,
                Container(
                  width: 220.0,
                  height: 115.0,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/images/reserve.png'))),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              FadeAnimation(
                2.6,
                Container(
                  padding: EdgeInsets.all(20.0),
                  width: 480.0,
                  height: 128.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    color: Color(0xFF2196F3),
                  ),
                  child: Column(
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            Icons.info,
                            size: 21.0,
                            color: Colors.white,
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Text(
                            "Don't Forget",
                            style: TextStyle(
                                fontSize: 18.0,
                                fontFamily: 'Monstserrat',
                                color: Colors.white,
                                fontWeight: FontWeight.w600),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Flexible(
                            child: Text(
                              "All bookings must be made within today's 24 hour clock",
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontFamily: 'Monstserrat',
                                  color: Colors.white,
                                  fontWeight: FontWeight.w300),
                            ),
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                          Constants.bookingInfoIcon()
                        ],
                      ),
                      SizedBox(
                        height: 8.0,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Flexible(
                            child: Text(
                              "We cancel bookings after 10 minutes of inactivity",
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontFamily: 'Monstserrat',
                                  color: Colors.white,
                                  fontWeight: FontWeight.w300),
                            ),
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                          Constants.bookingInfoIcon()
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _submitBooking(startTime, endTime) async {
    String userEcard;
    String encryptedEmail;

    try {
      encryptedEmail = AesService().encrypter(_userEmail);
      _servicesObj.getUser(encryptedEmail).then((QuerySnapshot user) {
        // If success
        userEcard = user.documents[0].data['ecard'];

        Navigator.of(context).pop();
        Map<String, dynamic> bookingInfo = {
          'charge_point': this._chargePoint,
          'charge_point_name': this._chargePointName,
          'ecard': userEcard,
          'email': encryptedEmail,
          'start_time': startTime,
          'end_time': endTime,
          'created_on': Timestamp.now(),
          'start_time_date':
              CreateReservationViewModel().formatTimeOfDay(_startPicked),
          'end_time_date':
              CreateReservationViewModel().formatTimeOfDay(_endPicked)
        };

        try {
          _servicesObj.addData(bookingInfo).then((_) {
            setState(() {
              _message =
                  'Your booking from $startTime to $endTime has been confirmed!';
              return showSuccessFlushbar(context, _message);
            });
          }).catchError((onError) {
            setState(() {
              _message = "Oops something went wrong, please try again!";
              return showErrorFlushbar(context, _message);
            });
          });
        } catch (e) {
          print(e);
        }
      });
    } catch (e) {
      print(e);
    }
  }
}
