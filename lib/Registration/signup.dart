import '../imports.dart';
import 'dart:ui';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  CrudServices servicesObj = CrudServices();

  Map<String, dynamic> userDetails = new Map();

  String _email, _password, _message, _ecard, _name, _surname;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final _passwordController = TextEditingController();

  final _auth = FirebaseAuth.instance;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Form(
          key: _formKey,
          child: Stack(
            children: <Widget>[
              Constants.welcomeBackground(context),
              Container(
                padding: EdgeInsets.all(10.0),
                child: Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.arrow_back),
                      color: Colors.white,
                      onPressed: () {
                        Navigator.popAndPushNamed(context, '/');
                      },
                    ),
                    Text(
                      'Sign Up',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontFamily: 'Monstserrat',
                          fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ),
              FadeAnimation(
                1.0,
                Center(
                  child: Container(
                    width: 400,
                    child: ListView(
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      children: <Widget>[
                        TextFormField(
                          style: Constants.registrationInputTextStyle(),
                          validator: Validators.compose([
                            Validators.required(
                                'Please enter your forename'),
                            Validators.maxLength(26,
                                'Your forename cannot be over 26 characters long'),
                            Validators.patternString(
                                r"^[\sA-Za-z]+$", 'Only alphabets are allowed'),
                          ]),
                          onSaved: (value) => _name = value,
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Color(0xFF51CBFF))),
                              hintText: 'Forename',
                              hintStyle: Constants.registrationHintTextStyle()),
                        ),
                        SizedBox(
                          height: 25.0,
                        ),
                        TextFormField(
                          style: Constants.registrationInputTextStyle(),
                          validator: Validators.compose([
                            Validators.required(
                                'Please enter your surname'),
                            Validators.maxLength(26,
                                'Your surname cannot be over 26 characters long'),
                            Validators.patternString(
                                r"^[\sA-Za-z]+$", 'Only alphabets are allowed'),
                          ]),
                          onSaved: (value) => _surname = value,
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Color(0xFF51CBFF))),
                              hintText: 'Surname',
                              hintStyle: Constants.registrationHintTextStyle()),
                        ),
                        SizedBox(
                          height: 25.0,
                        ),
                        TextFormField(
                          style: Constants.registrationInputTextStyle(),
                          keyboardType: TextInputType.emailAddress,
                          validator: Validators.compose([
                            Validators.required(
                                'Please enter an email address'),
                            Validators.email(
                                'Invalid email address, please use the form - test@example.com')
                          ]),
                          onSaved: (value) => _email = value,
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Color(0xFF51CBFF))),
                              hintText: 'Email',
                              hintStyle: Constants.registrationHintTextStyle()),
                        ),
                        SizedBox(
                          height: 25.0,
                        ),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          style: Constants.registrationInputTextStyle(),
                          onSaved: (value) => _ecard = value,
                          validator: Validators.compose([
                            Validators.required(
                                'Please provide your ecard number'),
                            Validators.minLength(
                                7, 'Your ecard must be 7 digits'),
                            Validators.maxLength(
                                7, 'Your ecard must be 7 digits'),
                            Validators.patternString(
                                r"^[0-9]+$", 'Only numbers are allowed'),
                          ]),
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Color(0xFF51CBFF))),
                              hintText: 'Point Access Card Number',
                              hintStyle: Constants.registrationHintTextStyle()),
                        ),
                        SizedBox(
                          height: 25.0,
                        ),
                        TextFormField(
                          style: Constants.registrationInputTextStyle(),
                          controller: _passwordController,
                          obscureText: true,
                          validator: Validators.compose([
                            Validators.required('Please enter a password'),
                            Validators.minLength(8,
                                'Your password must be at least 8 characters long'),
                            Validators.maxLength(128,
                                'Your password cannot be more than 128 characters long'),
                            Validators.patternString('(?=.*[A-Z])',
                                'Your password must contain at least 1 uppercase letter'),
                            Validators.patternString('(?=.*\\d)',
                                'Your password must contain at least 1 digit'),
                            Validators.patternString('(?=.*[a-z])',
                                'Your password must contain at least 1 lowercase letter'),
                            Validators.patternString('(?=.*[@#%&])',
                                'Your password must contain at least 1 special character - [@, #, % or &]')
                          ]),
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Color(0xFF51CBFF))),
                              hintText: 'Password',
                              hintStyle: Constants.registrationHintTextStyle()),
                        ),
                        SizedBox(
                          height: 25.0,
                        ),
                        TextFormField(
                          style: Constants.registrationInputTextStyle(),
                          obscureText: true,
                          validator: (value) {
                            String message;
                            if (value.isEmpty) {
                              message = 'Please confirm your password';
                            } else if (value != _passwordController.text) {
                              message = 'Your passwords do not match';
                            }
                            return message;
                          },
                          onSaved: (value) => _password = value,
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Color(0xFF51CBFF))),
                              hintText: 'Confirm Password',
                              hintStyle: Constants.registrationHintTextStyle()),
                        ),
                        SizedBox(
                          height: 60.0,
                        ),
                        Container(
                          width: 400,
                          child: FlatButton(
                            shape: new RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              side: BorderSide(color: Color(0xFF4ABD61)),
                            ),
                            color: Color(0xFF4ABD61),
                            textColor: Colors.white,
                            padding: EdgeInsets.all(15.0),
                            onPressed: () {
                              _submitCommand();
                            },
                            child: Text(
                              "Create Account",
                              style: Constants.buttonTextStyle(),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          )),
    );
  }

  void _createUserObject() {
    // Encrypting all data in the Map using AES
    userDetails['forename'] = AesService().encrypter(_name);
    userDetails['surname'] = AesService().encrypter(_surname);
    userDetails['ecard'] = AesService().encrypter(_ecard);
    userDetails['email'] = AesService().encrypter(_email);
  }

  void _submitCommand() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      _registrationCommand();
    }
  }

  Future<void> _registrationCommand() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      try {
        final newUser = await _auth.createUserWithEmailAndPassword(
            email: _email, password: _password);
        if (newUser != null) {
          try {
            await newUser.user.sendEmailVerification().then((val) async {
              _createUserObject();
              await servicesObj.createUser(newUser.user.uid, userDetails);
              Navigator.pushReplacementNamed(context, '/login');
              setState(() {
                _message =
                    "Almost there, we just need you to follow the instructions in the email we've sent!";
                return showSuccessFlushbar(context, _message);
              });
            });
          } catch (e) {
            setState(() {
              _message = 'An error has occured, please check your credentials!';
              return showErrorFlushbar(context, _message);
            });
          }
        }
      } catch (e) {
        setState(() {
          _message = 'An error has occured, please check your credentials!';
          return showErrorFlushbar(context, _message);
        });
      }
    }
  }
}
