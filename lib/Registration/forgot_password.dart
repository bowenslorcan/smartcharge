/*
Title: (Ep 21) Flutter: Password Reset Using Firebase Auth
Author: 1ManStartup
Date: Jul 31, 2019
Code version: 1
Availability: https://www.youtube.com/watch?v=w0rfZm6a3Hs
*/

import '../imports.dart';

class ForgotPasswordPage extends StatefulWidget {
  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  String _email;

  CrudServices servicesObj = new CrudServices();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Future<void> _resetCommand() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      ForgotPasswordViewModel().validateResetRequest(_email).then((val) {
        if (val == 'OK') {
          setState(() {
            return showSuccessFlushbar(context, 'A password reset link has been sent to your email!');
          });
        } else {
          setState(() {
            return showErrorFlushbar(context, val);
          });
        }
      });
    } else {
      setState(() {
        return showErrorFlushbar(context, 'Something went wrong, please try again');
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Form(
          key: _formKey,
          child: Stack(
            children: <Widget>[
              Constants.welcomeBackground(context),
              Container(
                padding: EdgeInsets.all(10.0),
                child: Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.arrow_back),
                      color: Colors.white,
                      onPressed: () {
                        Navigator.popAndPushNamed(context, '/login');
                      },
                    ),
                    Text(
                      'Forgot Password',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontFamily: 'Monstserrat',
                          fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ),
              Center(
                child: Container(
                  width: 400,
                  child: ListView(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    children: <Widget>[
                      FadeAnimation(1.0, TextFormField(
                          style: Constants.registrationInputTextStyle(),
                          keyboardType: TextInputType.emailAddress,
                          validator: Validators.compose([
                            Validators.required('Please enter an email address'),
                            Validators.email(
                                'Invalid email address, please use the form - test@example.com')
                          ]),
                          onSaved: (value) => _email = value,
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Color(0xFF51CBFF))),
                              hintText: 'Email',
                              hintStyle: Constants.registrationHintTextStyle()),
                        ),
                      ),
                      SizedBox(
                        height: 60.0,
                      ),
                      FadeAnimation(1.5, Container(
                          width: 400,
                          child: FlatButton(
                            shape: new RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              side: BorderSide(color: Color(0xFF4ABD61)),
                            ),
                            color: Color(0xFF4ABD61),
                            textColor: Colors.white,
                            padding: EdgeInsets.all(15.0),
                            onPressed: () {
                              _resetCommand();
                            },
                            child: Text(
                              "Submit",
                              style: Constants.buttonTextStyle(),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
