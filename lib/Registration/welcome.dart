import 'package:smart_charge/imports.dart';
import 'dart:ui';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Constants.welcomeBackground(context),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(bottom: 50),
                  child: FadeAnimation(
                    0.8,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Smart',
                          style: TextStyle(
                              fontSize: 48.0,
                              letterSpacing: 1.0,
                              fontWeight: FontWeight.w700,
                              color: Color(0xFF4ABD61)),
                        ),
                        Text(
                          'Charge',
                          style: TextStyle(
                              fontSize: 48.0,
                              letterSpacing: 1.0,
                              fontWeight: FontWeight.w700,
                              color: Colors.white),
                        )
                      ],
                    ),
                  ),
                ),
                Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    FadeAnimation(
                      1.0,
                      Container(
                        height: 120,
                        width: 200,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage('assets/images/Landing.png'),
                          ),
                        ),
                      ),
                    ),
                    FadeAnimation(
                      1.5,
                      Container(
                        width: 400,
                        padding: EdgeInsets.only(top: 135),
                        child: FlatButton(
                          shape: new RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0),
                            side: BorderSide(color: Color(0xFF4ABD61)),
                          ),
                          color: Color(0xFF4ABD61),
                          textColor: Colors.white,
                          padding: EdgeInsets.all(15.0),
                          onPressed: () {
                            Navigator.pushNamed(context, '/signup');
                          },
                          child: Text("Sign Up",
                              style: Constants.buttonTextStyle()),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 60.0,
                ),
                FadeAnimation(
                  2.0,
                  Container(
                    width: 400,
                    child: FlatButton(
                      shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        side: BorderSide(color: Color(0xFF4ABD61)),
                      ),
                      color: Color(0xFF4ABD61),
                      textColor: Colors.white,
                      padding: EdgeInsets.all(15.0),
                      onPressed: () {
                        Navigator.pushNamed(context, '/login');
                      },
                      child: Text("Login", style: Constants.buttonTextStyle()),
                    ),
                  ),
                )
              ],
            ),
          ],
        ));
  }

  void navigateToLogin() {
    Navigator.pushNamed(context, '/login');
  }

  void navigateToSignUp() {
    Navigator.pushNamed(context, '/signup');
  }
}
