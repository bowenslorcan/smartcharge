import '../imports.dart';
import 'dart:ui';

class PasswordFieldValidator {
  static String validatePassword(String value) {
    if (value.isEmpty) {
      return 'Please provide a password';
    }
    return null;
  }
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  FirebaseUser currentUser;
  FirebaseAuth _auth = FirebaseAuth.instance;

  // We use underscores to mark variables as private in the Dart language
  String _email, _password, _message;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Form(
          key: _formKey,
          child: Stack(
            children: <Widget>[
              Constants.welcomeBackground(context),
              Container(
                padding: EdgeInsets.all(10.0),
                child: Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.arrow_back),
                      color: Colors.white,
                      onPressed: () {
                        Navigator.popAndPushNamed(context, '/');
                      },
                    ),
                    Text(
                      'Login',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontFamily: 'Monstserrat',
                          fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ),
              Center(
                child: Container(
                  width: 400,
                  child: ListView(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    children: <Widget>[
                      FadeAnimation(
                        1.0,
                        TextFormField(
                          style: Constants.registrationInputTextStyle(),
                          keyboardType: TextInputType.emailAddress,
                          validator: Validators.compose([
                            Validators.required(
                                'Please enter an email address'),
                            Validators.email(
                                'Invalid email address, please use the form - test@example.com')
                          ]),
                          onSaved: (value) => _email = value,
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Color(0xFF51CBFF))),
                              hintText: 'Email',
                              hintStyle: Constants.registrationHintTextStyle()),
                        ),
                      ),
                      SizedBox(
                        height: 25.0,
                      ),
                      FadeAnimation(
                        1.5,
                        TextFormField(
                          style: Constants.registrationInputTextStyle(),
                          obscureText: true,
                          validator: (value) =>
                              PasswordFieldValidator.validatePassword(value),
                          onSaved: (value) => _password = value,
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Color(0xFF51CBFF))),
                              hintText: 'Password',
                              hintStyle: Constants.registrationHintTextStyle()),
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      FadeAnimation(
                        2.0,
                        Center(
                          child: Container(
                              alignment: Alignment.centerRight,
                              child: InkWell(
                                onTap: () {
                                  Navigator.pushNamed(
                                      context, '/forgot_password');
                                },
                                child: Text(
                                  'Forgot Password?',
                                  style: TextStyle(
                                      color: Color(0xFF51CBFF),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16.0),
                                ),
                              )),
                        ),
                      ),
                      SizedBox(
                        height: 60.0,
                      ),
                      FadeAnimation(
                        2.5,
                        Container(
                          width: 400,
                          child: FlatButton(
                            shape: new RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              side: BorderSide(color: Color(0xFF4ABD61)),
                            ),
                            color: Color(0xFF4ABD61),
                            textColor: Colors.white,
                            padding: EdgeInsets.all(15.0),
                            onPressed: () {
                              _loginCommand();
                            },
                            child: Text(
                              "Login",
                              style: Constants.buttonTextStyle(),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          )),
    );
  }

  Future<void> _loginCommand() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      try {
        final user = await _auth.signInWithEmailAndPassword(
            email: _email, password: _password);
        if (user != null) {
          if (user.user.isEmailVerified) {
            Navigator.pushReplacementNamed(context, '/mainhub');
          } else {
            setState(() {
              _message =
                  "Please review the email we've sent to compete your account setup!";
              return showErrorFlushbar(context, _message);
            });
          }
        }
      } catch (e) {
        print(e);
        setState(() {
          _message = 'An error has occured, please check your credentials!';
          return showErrorFlushbar(context, _message);
        });
      }
    }
  }
}
