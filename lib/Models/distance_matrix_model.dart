/*
Title: Distance Matrix API - MapResponse.dart
Author: Prasad Sunny
Date: April 26, 2020
Code version: 1
Availability: https://stackoverflow.com/questions/61166994/flutter-distancematrix-json-wont-parse-correctly
*/

import 'dart:convert';

class MapResponse {
  final List<String> destinationAddresses;
  final List<String> originAddresses;
  final List<RowDto> rows;
  final String status;

  MapResponse({
    this.destinationAddresses,
    this.originAddresses,
    this.rows,
    this.status,
  });

  factory MapResponse.fromJson(String str) =>
      MapResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory MapResponse.fromMap(Map<String, dynamic> json) => MapResponse(
        destinationAddresses: json["destination_addresses"] == null
            ? null
            : List<String>.from(json["destination_addresses"].map((x) => x)),
        originAddresses: json["origin_addresses"] == null
            ? null
            : List<String>.from(json["origin_addresses"].map((x) => x)),
        rows: json["rows"] == null
            ? null
            : List<RowDto>.from(json["rows"].map((x) => RowDto.fromMap(x))),
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toMap() => {
        "destination_addresses": destinationAddresses == null
            ? null
            : List<dynamic>.from(destinationAddresses.map((x) => x)),
        "origin_addresses": originAddresses == null
            ? null
            : List<dynamic>.from(originAddresses.map((x) => x)),
        "rows": rows == null
            ? null
            : List<dynamic>.from(rows.map((x) => x.toMap())),
        "status": status == null ? null : status,
      };
}

class RowDto {
  final List<Location> elements;

  RowDto({
    this.elements,
  });

  factory RowDto.fromJson(String str) => RowDto.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory RowDto.fromMap(Map<String, dynamic> json) => RowDto(
        elements: json["elements"] == null
            ? null
            : List<Location>.from(
                json["elements"].map((x) => Location.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "elements": elements == null
            ? null
            : List<dynamic>.from(elements.map((x) => x.toMap())),
      };
}

class Location {
  final Distance distance;
  final Distance duration;
  final String status;

  Location({
    this.distance,
    this.duration,
    this.status,
  });

  factory Location.fromJson(String str) => Location.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Location.fromMap(Map<String, dynamic> json) => Location(
        distance: json["distance"] == null
            ? null
            : Distance.fromMap(json["distance"]),
        duration: json["duration"] == null
            ? null
            : Distance.fromMap(json["duration"]),
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toMap() => {
        "distance": distance == null ? null : distance.toMap(),
        "duration": duration == null ? null : duration.toMap(),
        "status": status == null ? null : status,
      };
}

class Distance {
  final String text;
  final int value;

  Distance({
    this.text,
    this.value,
  });

  factory Distance.fromJson(String str) => Distance.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Distance.fromMap(Map<String, dynamic> json) => Distance(
        text: json["text"] == null ? null : json["text"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String, dynamic> toMap() => {
        "text": text == null ? null : text,
        "value": value == null ? null : value,
      };
}