import 'package:flutter/cupertino.dart';
import 'package:smart_charge/Models/distance_matrix_model.dart';
import 'package:http/http.dart' as http;

class RouteDetailsViewModel extends ChangeNotifier {
  var distanceMatrix = [];

  Future<List> getDistance(currentCoord, chargePointCoord, chargePoint) async {
    String point = chargePoint;

    String url =
        "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=$currentCoord&destinations=$chargePointCoord&key=AIzaSyBnV5_2E6SkunNQ2JwbrR7_1XUQK8s8cMo";

    http.Response res = await http.get(url);

    final response = MapResponse.fromJson(res.body);

    if (res.statusCode == 200) {
      var distance =
          response.rows.elementAt(0).elements.elementAt(0).distance.text;
      var duration =
          response.rows.elementAt(0).elements.elementAt(0).duration.text;

      distanceMatrix.add(distance);
      distanceMatrix.add(duration);
      distanceMatrix.add(point);
    } else {
      Exception('Failed');
    }
    return distanceMatrix;
  }
}
