import '../imports.dart';

class CreateReservationViewModel extends ChangeNotifier {
  // Prep timestamp
  DateTime formatTimeOfDay(TimeOfDay tod) {
    final now = new DateTime.now();
    final dt = DateTime(now.year, now.month, now.day, tod.hour, tod.minute);
    return dt;
  }

  CrudServices _servicesObj = new CrudServices();

  String _message;

  Future<String> bookingValidation(
      String stringStartTime,
      String stringEndTime,
      TimeOfDay _startPicked,
      TimeOfDay _endPicked,
      String _chargePoint,
      String _chargeType) async {
    DateTime timeNow = new DateTime.now();

    if (_startPicked == null || _endPicked == null) {
      _message = 'Please select a start and end time!';
    } else {
      await _servicesObj
          .findNextBooking(_chargePoint)
          .then((QuerySnapshot docs) {
        DateTime selectedStartDate = formatTimeOfDay(_startPicked);
        DateTime selectedEndDate = formatTimeOfDay(_endPicked);

        int duration;
        String bookingStartTime, nextBookingStartTime;

        DateTime bookingStartDateTime,
            bookingEndDateTime,
            nextBookingStartDateTime,
            nextBookingEndDateTime;

        bool currentTimeDifference = timeNow.isBefore(selectedStartDate);

        Duration startEndTimeDifference =
            selectedEndDate.difference(selectedStartDate);
        var startEndTimeValidator = startEndTimeDifference.inMinutes;

        // Start time or end time cannot be less than the current time now
        if (currentTimeDifference == false) {
          _message = 'Your booking must start now or at a later time!';
        }
        // Start time cannot be less than or equal to end time
        else if (startEndTimeValidator < 10) {
          _message = 'Your booking must be at least 10 minutes!';
        } else if (_chargeType == 'Standard Type 2' &&
            startEndTimeValidator > 360) {
          // More than 6 hours
          _message =
              'This is a $_chargeType point, you cannot have a booking greater than 6 hours!';
        } else if (_chargeType != 'Standard Type 2' &&
            startEndTimeValidator > 60) {
          // More than an hour for Fast Charger
          _message =
              'This is a $_chargeType point, you cannot have a booking greater than an hour!';
        } else if (docs.documents.isNotEmpty) {
          if (docs.documents.length > 1) {
            // Iterating through the list of bookings consecutively
            for (int i = 0; i < docs.documents.length; i += 1) {
              bookingStartTime = docs.documents[0].data['start_time'];
              bookingStartDateTime =
                  docs.documents[0].data['start_time_date'].toDate();
              bookingEndDateTime =
                  docs.documents[i].data['end_time_date'].toDate();

              // Storing result of next booking
              if (docs.documents.length > i + 1) {
                nextBookingStartTime = docs.documents[i + 1].data['start_time'];
                nextBookingStartDateTime =
                    docs.documents[i + 1].data['start_time_date'].toDate();
                nextBookingEndDateTime =
                    docs.documents[i + 1].data['end_time_date'].toDate();
              }

              // Calculating difference between current booking end time and next booking start time
              Duration difference =
                  nextBookingStartDateTime.difference(bookingEndDateTime);

              duration = difference.inMinutes;

              if (duration > 0) {
                // First booking check
                bool initialEndValidator =
                    selectedEndDate.isAfter(bookingStartDateTime);

                // Next consecutive booking check
                bool startValidator =
                    selectedStartDate.isAfter(bookingEndDateTime);
                bool endValidator =
                    selectedEndDate.isAfter(nextBookingStartDateTime);

                if (initialEndValidator == true) {
                  _message =
                      "I'm sorry, there's a booking starting at $bookingStartTime";
                  break;
                } else if (startValidator == true && endValidator == true) {
                  bool startValidator =
                      selectedStartDate.isAfter(nextBookingEndDateTime);

                  if (startValidator == true) {
                    _message = 'OK';
                    break;
                  }
                  _message =
                      "I'm sorry, there's a booking starting at $nextBookingStartTime";
                  break;
                } else {
                  _message = 'OK';
                  break;
                }
              }
            }
          } else {
            bookingStartTime = docs.documents[0].data['start_time'];
            bookingStartDateTime =
                docs.documents[0].data['start_time_date'].toDate();
            bookingEndDateTime =
                docs.documents[0].data['end_time_date'].toDate();

            bool startValidator =
                selectedStartDate.isAfter(bookingStartDateTime);
            bool endValidator = selectedEndDate.isBefore(bookingEndDateTime);

            if (startValidator == true && endValidator == true) {
              _message =
                  "I'm sorry, there's a booking starting at $bookingStartTime";
            } else {
              _message = 'OK';
            }
          }
        } else {
          _message = 'OK';
        }
      });
    }
    return _message;
  }
}
