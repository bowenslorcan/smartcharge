import '../imports.dart';

class PointDetailsViewModel extends ChangeNotifier {
  DateTime currentTime = DateTime.now();
  CrudServices _servicesObj = CrudServices();
  String availability;

  // Find Available Bookings
  Future<String> findNextAvailableBooking(String pointID) async {

    await _servicesObj.getChargePointStatus(pointID).then((DocumentSnapshot status) async {
      bool pointStatus = status.data['occupied'];
      await _servicesObj.findNextBooking(pointID).then((QuerySnapshot bookings) {
        // Business Logic
        if (bookings.documents.isNotEmpty) {
          DateTime firstBookingReceived;
          String firstBookingReceivedEndTime;
          int gap;

          firstBookingReceived =
              bookings.documents[0].data['start_time_date'].toDate();
          firstBookingReceivedEndTime = bookings.documents[0].data['end_time'];

          Duration gapDifference = firstBookingReceived.difference(currentTime);
          gap = gapDifference.inMinutes;

          if (bookings.documents.length == 1) {
            if (gap >= 15 || gap < 0) {
              if (pointStatus == true) {
                availability = 'Immediately after current session';
              }
              else {
                availability = 'Now';
              }
            } else {
              availability = firstBookingReceivedEndTime;
            }
          } else if (bookings.documents.length > 1) {
            if (gap >= 15 || gap < 0) {
              if (pointStatus == true) {
                availability = 'Immediately after current session';
              }
              else {
                availability = 'Now';
              }
            } else {
              availability = firstBookingReceivedEndTime;
            }
          } else {
            try {
              int duration;
              String bookingEndTime, nextBookingEndTime;
              DateTime nextBookingDateTime, bookingDateTime;

              // Iterating through the list of bookings consecutively
              for (int i = 0; i < bookings.documents.length; i += 1) {
                bookingDateTime =
                    bookings.documents[i].data['end_time_date'].toDate();

                // Storing result of next booking
                if (bookings.documents.length > i + 1) {
                  nextBookingDateTime =
                      bookings.documents[i + 1].data['start_time_date'].toDate();
                  nextBookingEndTime = bookings.documents[i + 1].data['end_time'];
                }

                // Calculating difference between current booking end time and next booking start time
                Duration difference =
                nextBookingDateTime.difference(bookingDateTime);

                duration = difference.inMinutes.abs();

                if (duration >= 10) {
                  availability = bookingEndTime;
                  break;
                }
              }

              if (duration < 10) {
                availability = nextBookingEndTime;
              }
            } catch (e) {
              print(e);
            }
          }
        } else {
          if (pointStatus == true) {
            availability = 'Immediately after current session';
          }
          else {
            availability = 'Now';
          }
        }
      });
    });
    return availability;
  }
}
