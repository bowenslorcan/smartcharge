import '../imports.dart';

class ForgotPasswordViewModel {
  CrudServices _servicesObj = CrudServices();

  String _message;

  Future<String> validateResetRequest(email) async {
    try {
      await _servicesObj.resetPasswordEmail(email).then((success) {
        _message = 'OK';
      }).catchError((onError) {
        if (onError is PlatformException) {
          switch (onError.code) {
            case 'ERROR_USER_NOT_FOUND':
              _message = 'An error has occured, please check your credentials!';
              break;
            case 'ERROR_INVALID_EMAIL':
              _message = 'An error has occured, please check your credentials!';
              break;
            case 'ERROR_USER_DISABLED':
              _message = 'This user has been disabled!';
              break;
            case 'ERROR_OPERATION_NOT_ALLOWED':
              _message = 'This operation is not allowed!';
              break;
            default:
              _message = 'An error has occured, please check your credentials!';
          }
        }
      });
    } catch (e) {
      _message = 'An error has occured, please check your credentials!';
    }
    return _message;
  }
}
