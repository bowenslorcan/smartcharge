import 'package:flutter/material.dart';
import 'package:smart_charge/Registration/login.dart';
import 'package:smart_charge/Registration/signup.dart';
import 'package:smart_charge/Registration/welcome.dart';
import 'package:smart_charge/Registration/forgot_password.dart';
import 'package:smart_charge/Screens/main_hub_navigation.dart';

void main() => runApp(SmartCharge());

class SmartCharge extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "SmartCharge",
      theme: ThemeData(
        fontFamily: 'Montserrat',
        primarySwatch: Colors.green,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => WelcomePage(),
        '/login': (context) => LoginPage(),
        '/signup': (context) => SignUpPage(),
        '/forgot_password': (context) => ForgotPasswordPage(),
        '/mainhub': (context) => MainHub()
      },
    );
  }
}
