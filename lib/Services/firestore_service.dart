/*
Title: Flutter - Firestore CRUD (Reading and writing)
Author: Raja Yogan
Date: Sep 6, 2018
Code version: 1
Availability: https://www.youtube.com/watch?v=ipa3T_gVe8U
*/

import '../imports.dart';

class CrudServices {
  final _auth = FirebaseAuth.instance;
  final _db = Firestore.instance;

  bool isLoggedIn() {
    if (_auth.currentUser() != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> logout() async {
    await _auth.signOut();
  }

  Future<void> resetPasswordEmail(email) async {
    return _auth.sendPasswordResetEmail(email: email);
  }

  Future<void> addData(bookingInfo) async {
    if (isLoggedIn()) {
      _db.collection('bookings').add(bookingInfo).catchError((e) {
        print(e);
      });
    } else {
      print('Please make sure you have logged in');
    }
  }

  Future<void> createUser(uid, userData) async {
    _db.collection('users').document(uid).setData(userData);
  }

  searchByName(String searchField) {
    return _db
        .collection('charge_points')
        .where('search_key',
            isEqualTo: searchField.substring(0, 1).toUpperCase())
        .getDocuments();
  }

  findNextBooking(String chargePointID) {
    return _db
        .collection('bookings')
        .where('charge_point', isEqualTo: chargePointID)
        .orderBy('start_time_date')
        .getDocuments();
  }

  initialiseChargePoints() {
    return _db.collection('charge_points').getDocuments();
  }

  getChargePoints() {
    return _db.collection('charge_points').snapshots();
  }

  getChargePointStatus(pointID) {
    return _db.collection('charge_points').document(pointID).get();
  }

  getUser(userEmail) {
    return _db
        .collection('users')
        .where('email', isEqualTo: userEmail)
        .getDocuments();
  }

  getUserBookings(userEmail) {
    return _db
        .collection('bookings')
        .where('email', isEqualTo: userEmail)
        .snapshots();
  }

  getAllBookings(pointID) {
    return _db
        .collection('bookings')
        .where('charge_point', isEqualTo: pointID)
        .orderBy('start_time_date')
        .snapshots();
  }

  Future<void> deleteBooking(snapshot, index) async {
    if (isLoggedIn()) {
      await _db.runTransaction((Transaction clearBooking) async {
        await clearBooking
            .delete(snapshot.data.documents[index].reference)
            .catchError((e) {
          print(e);
        });
      });
    }
  }
}
