import '../imports.dart';

class ViewAllBookingsDialog extends StatefulWidget {
  final chargePointID;
  final pointName;

  ViewAllBookingsDialog({this.chargePointID, this.pointName});

  @override
  _ViewAllBookingsDialogState createState() => _ViewAllBookingsDialogState();
}

class _ViewAllBookingsDialogState extends State<ViewAllBookingsDialog> {
  CrudServices _servicesObj = CrudServices();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Bookings for ' + widget.pointName),
      content: Container(
        color: Colors.white,
        height: 300.0,
        width: MediaQuery.of(context).size.width,
        child: StreamBuilder(
            stream: _servicesObj.getAllBookings(widget.chargePointID),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Spinner();
              } else if (snapshot.data.documents.length > 0) {
                return ListView(
                  children: <Widget>[
                    ListView(
                      primary: false,
                      shrinkWrap: true,
                      padding: EdgeInsets.only(left: 25.0, right: 25.0),
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 10.0),
                          height: MediaQuery.of(context).size.height - 300.0,
                          child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: snapshot.data.documents.length,
                            itemBuilder: (_, index) {
                              return _buildBookings(snapshot, index);
                            },
                          ),
                        )
                      ],
                    )
                  ],
                );
              } else {
                return Center(
                    child: Text(
                  'There are no bookings for this point!',
                  style: Constants.informationTextStyleHeading(),
                ));
              }
            }),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('Close'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }

  Widget _buildBookings(AsyncSnapshot snapshot, int index) {
    final bookingDetails = snapshot.data.documents[index];
    return Container(
      padding: EdgeInsets.only(bottom: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Container(
              width: MediaQuery.of(context).size.width - 100.0,
              child: Container(
                padding: EdgeInsets.only(left: 10.0, right: 10.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          height: 30.0,
                          width: 30.0,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/clock_icon.png'))),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text(
                          'Duration : ' +
                              bookingDetails.data['start_time'].toString() +
                              ' - ' +
                              bookingDetails.data['end_time'].toString(),
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16.0,
                              fontFamily: 'Monstserrat',
                              fontWeight: FontWeight.w400),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Divider()
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
