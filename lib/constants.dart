import 'imports.dart';
import 'dart:ui';

class Constants {
  static informationTextStyleBody() {
    return TextStyle(
        color: Colors.black,
        fontSize: 16.0,
        fontFamily: 'Monstserrat',
        fontWeight: FontWeight.w400);
  }

  static informationTextStyleHeading() {
    return TextStyle(
        color: Colors.black,
        fontSize: 20.0,
        fontFamily: 'Monstserrat',
        fontWeight: FontWeight.w600);
  }

  static buttonTextStyle() {
    return TextStyle(
        fontSize: 16.0, fontFamily: 'Monstserrat', fontWeight: FontWeight.w600);
  }

  static hintTextStyle() {
    return TextStyle(
        color: Colors.black,
        fontSize: 16.0,
        fontFamily: 'Monstserrat',
        fontWeight: FontWeight.w300);
  }

  static registrationInputTextStyle() {
    return TextStyle(
        color: Colors.white,
        fontSize: 16.0,
        fontFamily: 'Monstserrat',
        fontWeight: FontWeight.w400);
  }

  static registrationHintTextStyle() {
    return TextStyle(
        color: Colors.white,
        fontSize: 16.0,
        fontFamily: 'Monstserrat',
        fontWeight: FontWeight.w300);
  }

  static bookingsTitle() {
    return Padding(
      padding: EdgeInsets.all(20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'My',
            style: TextStyle(
                color: Color(0xFF4ABD61),
                fontSize: 24.0,
                fontFamily: 'Monstserrat',
                fontWeight: FontWeight.w500),
          ),
          Text(
            'Bookings',
            style: TextStyle(
                color: Color(0xFF0D2A47),
                fontSize: 24.0,
                fontFamily: 'Monstserrat',
                fontWeight: FontWeight.w500),
          ),
          Text(
            '.',
            style: TextStyle(
                color: Color(0xFF4ABD61),
                fontSize: 24.0,
                fontFamily: 'Monstserrat',
                fontWeight: FontWeight.w500),
          )
        ],
      ),
    );
  }

  static welcomeBackground(context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage('assets/images/Space_Gradient_Background.jpg'),
          ),
        ),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 13.0, sigmaY: 13.0),
          child: Container(
            color: Colors.black.withOpacity(0.55),
          ),
        ));
  }

  static bookingInfoIcon() {
    return Container(
      height: 20.0,
      width: 20.0,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/hand_icon.png'))),
    );
  }
}
